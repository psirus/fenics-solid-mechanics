#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <dolfin/log/log.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/function/Function.h>

#include <fenics-solid-mechanics/StateUpdate.h>
#include <fenics-solid-mechanics/ConstitutiveUpdate.h>
#include <fenics-solid-mechanics/PlasticityModel.h>

namespace py = pybind11;
namespace fsm = fenicssolid;

PYBIND11_MAKE_OPAQUE(std::vector<double>);

namespace fenicssolid_wrappers
{
void status_update(py::module& m)
{
    py::class_<std::vector<double>>(m, "DoubleVector")
        .def(py::init<>())
        .def("clear", &std::vector<double>::clear)
        .def("pop_back", &std::vector<double>::pop_back)
        .def("__len__", [](const std::vector<double>& v) { return v.size(); })
        .def("__iter__", [](std::vector<double>& v) { return py::make_iterator(v.begin(), v.end()); },
             py::keep_alive<0, 1>()); /* Keep vector alive while iterator is used */
    py::class_<fsm::StateUpdate, std::shared_ptr<fsm::StateUpdate>>(m, "StateUpdate", "Base class StateUpdate")
        .def("update", &fsm::StateUpdate::update);
    py::class_<fsm::ConstitutiveUpdate, std::shared_ptr<fsm::ConstitutiveUpdate>, fsm::StateUpdate>(
        m, "ConstitutiveUpdate", "Class ConstitutiveUpdate")
        .def(py::init<std::shared_ptr<const dolfin::Function>, std::shared_ptr<const dolfin::FiniteElement>,
                      std::shared_ptr<const dolfin::GenericDofMap>, std::shared_ptr<const fsm::PlasticityModel>>(),
             "Create a ConstitutiveUpdate instance")
        //       .def("update", &fsm::ConstitutiveUpdate::update)
        .def("update_history", &fsm::ConstitutiveUpdate::update_history)
        .def("w_stress", &fsm::ConstitutiveUpdate::w_stress, py::return_value_policy::reference_internal)
        .def("w_tangent", &fsm::ConstitutiveUpdate::w_tangent, py::return_value_policy::reference_internal)
        .def("eps_p_eq", &fsm::ConstitutiveUpdate::eps_p_eq, py::return_value_policy::reference_internal);
}
}
