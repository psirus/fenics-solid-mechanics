// This variable is a characteristic length and it controles the mesh size around a Point.
// It is possible to specify more than one variable for this purpose.
cl = 0.135;

// Points contains the x, y and z coordinate and the characteristic length of the Point.
Point(1) = {0,0,0,cl};
Point(2) = {5,0,0,cl};
Point(3) = {5,1,0,cl};
Point(4) = {0,1,0,cl};
Point(5) = {0,0,1,cl};
Point(6) = {5,0,1,cl};
Point(7) = {5,1,1,cl};
Point(8) = {0,1,1,cl};
Point(9) = {0,0,0.5,cl};
Point(10) = {5,0,0.5,cl};
Point(11) = {5,1,0.5,cl};
Point(12) = {0,1,0.5,cl};

// A Line is basically a connection between two Points. A good practice is to connect the
// Points in a counter-clockwise fashion.
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,5};

Line(9) = {10,11};
Line(10) = {12,9};

Line(11) = {1,9};
Line(12) = {9,5};

Line(13) = {2,10};
Line(14) = {10,6};

Line(15) = {3,11};
Line(16) = {11,7};

Line(17) = {4,12};
Line(18) = {12,8};

// A Line Loop is the connection of Lines that defines an area. Again it is good practice
// to do this in a counter-clockwise fashion.
Line Loop(1) = {-1,-4,-3,-2};
Line Loop(2) = {1,13,14,-5,-12,-11};
Line Loop(3) = {2,15,-9,-13};
Line Loop(4) = {9,16,-6,-14};
Line Loop(5) = {3,17,18,-7,-16,-15};
Line Loop(6) = {4,11,-10,-17};
Line Loop(7) = {10,12,-8,-18};
Line Loop(8) = {5,6,7,8};

// From the Line Loop it is now possible to create a surface, in this case a Plane Surface.
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7};
Plane Surface(8) = {8};

// Surface loop
Surface Loop(1) = {1,2,3,4,5,6,7,8};

// From the Plane Surface a Physical Surface is generated, this makes is possible to only
// save elements which are defined on the area specified by the Line Loop.
Volume(1) = {1};
Physical Volume(1) = {1};
